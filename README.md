# Ghost Announcer

Announces new posts on slack, rocket chat or discord (and potentially 
other chat platforms ) when a ghost blog tells it. 
May also work with other similar push webhook systems

## Instructions for use

Clone the repo 

Get the modules

```
npm install
```
create a credentials.json with the correct urls and so on (see example)

test the credentials
```
node testargs.js 
```
if it worked add some feeds to sources.json (again see example)

run the main script

```
node index.js
```
