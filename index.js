/* Ghost Announcer (c) 2021 Hellokitty@goatse.cx
 * 
 * This script creates a webseerver that ghost can post updates to. It 
 * then announces thouse updates to the various channels in credentials.json
 */
 
 'use strict';

const http = require('http');
const url = require('url');

const fetch = require('node-fetch'); // for fetching the feed
const Discord = require('discord.js'); // announcing to discord
const { IncomingWebhook } = require('@slack/webhook'); // announcing to slack

var creds = require('./credentials.json'); 

function rockethook(msg, url) {
	var body = {"text":msg}

	return fetch(url, {
			method: 'post',
			body:    JSON.stringify(body),
			headers: { 'Content-Type': 'application/json' },
		});
}

// create hooks once 
for (var i=0; i<creds.length; i++) {
	switch(creds[i].service) {
		case "slack":
			creds[i]["hook"]=new IncomingWebhook(creds[i].url);
		    break;
//		case "rocket":
//		    creds[i]["hook"]=rockethook;
//		    break;
		case "discord":
			// match https://discord.com/api/webhooks/12345678910/T0kEn0fw3Bh00K
			var m=creds[i].url.match(/webhooks\/([^\/]+)\/(.+)/);
		    creds[i]["hook"] = new Discord.WebhookClient(m[1], m[2]);
		    break;
	} 
}

function announcement(text) {
	for (var i=0; i<creds.length; i++) {
		switch(creds[i].service) {
			case "slack":
				creds[i].hook.send({text: text})
				.then(console.log("sent slack "+ text))
			    .catch(console.error);
				break;
			case "rocket":
				rockethook(text,creds[i].url)
				.then(console.log("sent rocket "+ text))
			    .catch(console.error);
				break;
			case "discord":
				creds[i].hook.send(text , {username: creds[i].postID})
				.then(console.log("sent discord "+text))
			    .catch(console.error);
				break;
		} 
	}
}


// Create http server.
var httpServer = http.createServer(function (req, resp) {

    // Get client request url.
    var reqUrlString = req.url;

    // Get client request path name.
    var pathName = url.parse(reqUrlString, true, false).pathname;
    var method = req.method;

    // If post to /discordant/ .
    if (('/announcer/' == pathName) && ("POST" == method))
    {

		var postData = '';

		// Get all post data when receive data event.
		req.on('data', function (chunk) {

			postData += chunk;

		});

		// When all request post data has been received.
		req.on('end', function () {

			
			resp.writeHead(200);
			resp.end('OK');

			// Parse the post data and get client sent username and password.
			var postDataObject = JSON.parse(postData);

			var title = postDataObject.post.current.title;

			var url = postDataObject.post.current.url;
			console.log("New meme : " + title + ": " + url);
			// Send a message using the webhook
			announcement(title +" - " + url );
		})
        
    } else {
		resp.writeHead(404);
		resp.end('FAIL');
	}
});

// Http server listen on port 3145.
httpServer.listen(3145);

console.log("Server is started.");
